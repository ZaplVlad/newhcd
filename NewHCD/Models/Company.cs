﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco;
using umbraco.cms.businesslogic.media;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Core.Dynamics;
using Umbraco.Core.Services;


namespace NewHCD.Models
{
    public enum CompanyTypes
    {
        Other, Private, Public
    }

    public struct HeaderContentLink
    {
        public string Header { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
    }

    public class Company
    {
        public int ID { get; private set; }
        public string Header { get; set; }

        #region Company data

        // Company information
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string ContactPerson { get; set; }
        public bool IsEmailValidated { get; set; }
        
        // What we offer
        public string WhatWeOffer { get; set; }

        // Products and solutions
        public List<HeaderContentLink> ProductsAndSolutions { get; set; }

        // Case stories
        public List<HeaderContentLink> CaseStories { get; set; }

        // Company facts
        public string Description { get; set; }
        public string NumberOfEmployeers { get; set; }
        public string FoundationYear { get; set; }
        public List<Media> CompanyImages { get; set; }

        // Add. information
        public CompanyTypes CompanyType { get; set; }
        public string CompanyClients { get; set; }
        public bool IsOpenForDelegations { get; set; }
        public string DelegationsAddress { get; set; }
        public string DelegationsZipCode { get; set; }
        public string DelegationsCity { get; set; }
        public string DelegationsPersonName { get; set; }
        public string DelegationsPersonEmail { get; set; }
        public string DelegationsPersonPhone { get; set; }

        // Comments
        public string Comments { get; set; }

        #endregion

        #region Methods

        public Company(int id)
        {
            var comp = uQuery.GetDocument(id);
            if (comp != null)
            {
                this.ID = id;
                this.Header = comp.Text;

                this.CompanyName = comp.getProperty("name").Value.ToString();
                this.Address = comp.getProperty("address").Value.ToString();
                this.ZipCode = comp.getProperty("zipCode").Value.ToString();
                this.City = comp.getProperty("city").Value.ToString();
                this.Email = comp.getProperty("email").Value.ToString();
                this.WebSite = comp.getProperty("webSite").Value.ToString();
                this.ContactPerson = comp.getProperty("contactPerson").Value.ToString();
                this.IsEmailValidated = (comp.getProperty("emailValidated").Value.ToString() == "Yes");

                this.WhatWeOffer = comp.getProperty("whatWeOffer").Value.ToString();

                this.ProductsAndSolutions = new List<HeaderContentLink>();
                var productsAndSolutionsXML = new DynamicXml((string)comp.getProperty("productsAndSolutions").Value);
                if (productsAndSolutionsXML.BaseElement != null)
                foreach (var item in productsAndSolutionsXML)
                {
                    HeaderContentLink hcl = new HeaderContentLink();
                    var xmlItem = item.BaseElement;
                    hcl.Header = xmlItem.Element("header").Value;
                    hcl.Content = xmlItem.Element("content").Value;
                    hcl.Link = xmlItem.Element("link").Value;
                    ProductsAndSolutions.Add(hcl);
                }

                this.CaseStories = new List<HeaderContentLink>();
                var caseStoriesXML = new DynamicXml((string)comp.getProperty("caseStories").Value);
                if (caseStoriesXML.BaseElement != null)
                foreach (var item in caseStoriesXML)
                {
                    HeaderContentLink hcl = new HeaderContentLink();
                    var xmlItem = item.BaseElement;
                    hcl.Header = xmlItem.Element("header").Value;
                    hcl.Content = xmlItem.Element("content").Value;
                    hcl.Link = xmlItem.Element("link").Value;
                    CaseStories.Add(hcl);
                }

                this.Description = comp.getProperty("description").Value.ToString();
                this.NumberOfEmployeers = comp.getProperty("numberOfEmployeers").Value.ToString();
                this.FoundationYear = comp.getProperty("foundationYear").Value.ToString();
                this.CompanyImages = new List<Media>();
                var companyImagesLinksXML = new DynamicXml((string)comp.getProperty("companyImages").Value);
                if (companyImagesLinksXML.BaseElement != null)
                foreach (var item in companyImagesLinksXML)
                {
                    var xmlItem = item.BaseElement;
                    var imgID = xmlItem.Element("image").Value;
                    var img = uQuery.GetMedia(int.Parse(imgID));

                    CompanyImages.Add(img);
                }

                string strType = comp.getProperty("companyType").Value.ToString();
                if (strType.Trim().ToLower() == "public")
                {
                    CompanyType = CompanyTypes.Public;
                }
                else if (strType.Trim().ToLower() == "private")
                {
                    this.CompanyType = CompanyTypes.Private;
                }
                else
                {
                    this.CompanyType = CompanyTypes.Other;
                }
                this.CompanyClients = comp.getProperty("yourClients").Value.ToString();
                this.IsOpenForDelegations = (comp.getProperty("delegations").Value.ToString() == "Yes");
                this.DelegationsAddress = comp.getProperty("delegationsAddress").Value.ToString();
                this.DelegationsCity = comp.getProperty("delegationsCity").Value.ToString();
                this.DelegationsPersonEmail = comp.getProperty("personEmail").Value.ToString();
                this.DelegationsPersonName = comp.getProperty("personName").Value.ToString();
                this.DelegationsPersonPhone = comp.getProperty("personPhone").Value.ToString();
                this.DelegationsZipCode = comp.getProperty("delegationsZipCode").Value.ToString();

                this.Comments = comp.getProperty("comments").Value.ToString();
            }
        }

        public static Company CreateNew(string header)
        {
            Node unpublishedFolder = null;
            var nodes = uQuery.GetNodesByType("CompaniesFolder");
            foreach (var node in nodes)
            {
                if (node.Name == "Unpublished Companies")
                {
                    unpublishedFolder = node;
                    break;
                }
            }

            if (unpublishedFolder != null)
            {
                ContentService contentService = new ContentService();
                var product = contentService.CreateContent(header, unpublishedFolder.Id, "Company", 0);
                contentService.Save(product);
                return new Company(product.Id);
            }
            return null;
        }

        //public void Save
        //{

        //}

        //public void SaveAndPublish
        //{

        //}

        //public static List<Company> GetAll()
        //{

        //}

        //public static List<Company> GetPublished()
        //{

        //}

        //public static List<Company> GetUnpublished()
        //{

        //}

        #endregion
    }
}