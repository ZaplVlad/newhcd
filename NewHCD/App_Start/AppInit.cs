﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

[assembly: PreApplicationStartMethod(typeof(NewHCD.App_Start.AppInit), "Init")]

namespace NewHCD.App_Start
{
    public static class AppInit
    {
        public static void Init()
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}